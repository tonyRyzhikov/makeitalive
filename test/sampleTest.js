/// <reference path="../typings/jasmine/jasmine.d.ts"/>
/// <reference path="../test/sampleData/test__person.ts"/>
describe("suite", function () {
    it("should be true", function () {
        expect(true).toBe(true);
    });
    it("should work with TypeScript", function () {
        var person = new Sample.Person("Joe");
        expect(person.name).toBe("Joe");
        person.name = "Jack";
        expect(person.name).toBe("Jack");
    });
});
//# sourceMappingURL=sampleTest.js.map