/// <reference path="../typings/jasmine/jasmine.d.ts"/>
/// <reference path="../test/sampleData/test__person.ts"/>
 
describe("suite", () => {
 it("should be true", () => {
  expect(true).toBe(true);
 });
 
 it("should work with TypeScript", () => {
  var person = new Sample.Person("Joe");
  expect(person.name).toBe("Joe");
  person.name = "Jack";
  expect(person.name).toBe("Jack");
 });
 
});