/// <reference path="../typings/angular2/angular2.d.ts"/>
import {Component, View, bootstrap, NgFor} from 'angular2/angular2';

class SampleObject{
  canDrink: boolean;  
}

interface ISampleComponent{
  myName: string;
  names: Array<string>;
  vodka: SampleObject;
}

// Annotation section
@Component({
  selector: 'my-app'
})
@View({
  template: `
  <p>My name: {{ myName }}</p>
  <p>Friends:</p>
  <ul>
     <li *ng-for="#name of names">
        {{ name }}
     </li>
  </ul>
`,
directives: [NgFor]
})

// Component controller
class AppComponent implements ISampleComponent {
    myName: string;
    names: Array<string>;
    vodka: SampleObject;
  constructor() {
    this.myName = 'Tony';
    this.names = ["Aarav", "Martín", "Shannon", "Ariana", "Kai"];
    this.vodka = <SampleObject>{};
  }
}

bootstrap(AppComponent);