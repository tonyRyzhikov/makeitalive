var gulp = require("gulp");
var ts = require("gulp-typescript");
var sourcemaps = require('gulp-sourcemaps');
var typeScript = require("typescript");
var path = require("path");
var Server = require('karma').Server;

var tscOptions = {
	"target": "ES5",
	"module": "commonjs",
	"sourceMap": true,
	"removeComments": true,
	"experimentalDecorators":true,
	"emitDecoratorMetadata": true,
	typescript: typeScript,
	isolatedModules: true
};

var getBase = function(file){
	return file.base;
}

var paths = {
	angular: 'angular/**/*.ts',
	node: 'routes/**/*.ts',
	test: 'test/**/*.ts'
}


var build = function(srcPattern){
	var links = gulp.src(srcPattern);
	var tsResult = links
		.pipe(sourcemaps.init())
		.pipe(ts(tscOptions));
	var result = tsResult.js
		.pipe(sourcemaps.write("./"))
		.pipe(gulp.dest(getBase));
	return result;
}

gulp.task('build-angular', function(){
	return build(paths.angular);
});
gulp.task('build-node', function(){
	return build(paths.node);
});
gulp.task('build-test', function(){
	return build(paths.test);
});

gulp.task('watch-build', function() {
  gulp.watch(paths.angular, ['build-angular']);
  gulp.watch(paths.node, ['build-node']);
  gulp.watch(paths.test, ['build-test']);
});

gulp.task('watch-run-tests', function(done){
	new Server({
    	configFile: __dirname + '/karma.conf.js'
  	}, done).start();
});



gulp.task('default', ['watch-build', 'watch-run-tests']);