/// <reference path='../typings/rx/node/node.d.ts' />
"use strict";
var express = require('express');
var path = require('path');
var router = express.Router();
var resolvedPath;
router.get('/', function (req, res, next) {
    resolvedPath = path.resolve(__dirname + '/../views/index.html');
    return res.sendFile(resolvedPath);
});
module.exports = router;
//# sourceMappingURL=index.js.map