/// <reference path='../typings/rx/node/node.d.ts' />
"use strict"

var express = require('express');
var path = require('path');
var router = express.Router();
var resolvedPath:string;

/* GET home page. */
router.get('/', (req, res, next) =>  {
	resolvedPath = path.resolve(__dirname + '/../views/index.html');
	return res.sendFile(resolvedPath);
});

module.exports = router;
