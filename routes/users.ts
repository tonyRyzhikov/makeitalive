/// <reference path='../typings/rx/node/node.d.ts' />
var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', (req, res, next) =>  res.send('respond with a resource'));

module.exports = router;
