# README #
[![bitHound Score](https://www.bithound.io/bitbucket/tonyRyzhikov/makeitalive/badges/score.svg)](https://www.bithound.io/bitbucket/tonyRyzhikov/makeitalive/master)
[ ![Codeship Status for tonyRyzhikov/makeItAlive](https://codeship.com/projects/79bb0240-0ea3-0133-73db-5acaa1e219b5/status?branch=master)](https://codeship.com/projects/91744)

Hi!

This is gonna be a simple MEAN app.
The more info about what will be added a bit later.
But -it's gonna be cool (trust me, I'm ~~a doctor~~ an engineer)

## Technology stack ##

* NodeJS + Express
* TypeScript 1.5.0-beta
* Angular 2.0 (ES5 for now)
* *Firebase (soon!)*
* *Gulp automation (soon!)*
* Karma + Jasmine unit testing

### Environment details ###

* Codeship CI
* [Heroku](https://makeitalive.herokuapp.com/) hosting 